<h1>Projet de cours UQAC - Pigeon Square</h1>

<p><h2>Description du travail</h2>
Jeu 2D dans lequel des pigeons se déplacent à la recherche de la nouriture posée par le joueur en cliquant.
La nourriture vieillit puis disparait. Les pigeons ne peuvent manger que la nourriture fraîche.
</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Eclipse
    <li>Java
    <li>Swing
</ul></p>

<p><h2>Participants</h2>
<ul><li>Ludovic Jozereau
</ul></p>

[Télécharger l'exécutable](https://gitlab.com/Jozereau_Ludovic/9inf957_PigeonSquare/raw/master/Pigeon%20Square.jar)